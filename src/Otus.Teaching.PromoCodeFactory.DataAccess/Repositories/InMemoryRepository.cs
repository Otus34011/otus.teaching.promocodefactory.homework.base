﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IList<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddAsync(T obj)
        {
            Data.Add(obj);

            return Task.FromResult(0);
        }

        public Task UpdateAsync(T obj)
        {
            var objForUpdate = Data.FirstOrDefault(x => x.Id == obj.Id);
            if(objForUpdate != null)             
                objForUpdate = obj;

            return Task.FromResult(0);
        }

        public Task DeleteAsync(Guid id)
        {
            var objForDelete = Data.FirstOrDefault(x => x.Id == id);
            if (objForDelete != null)
                Data.Remove(objForDelete);

            return Task.FromResult(0);
        }
    }
}