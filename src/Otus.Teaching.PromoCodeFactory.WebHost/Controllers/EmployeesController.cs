﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeShortResponse>> CreateEmployee(EmployeeCreateUpdateRequest request)
        {
            try
            {
                if (request == null)
                    return BadRequest();

                var employee = new Employee()
                {
                    Id = Guid.NewGuid(),
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email,
                    AppliedPromocodesCount = request.AppliedPromocodesCount,
                    Roles = request.Roles.Select(x => new Role()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Description
                    }).ToList()
                };

                await _employeeRepository.AddAsync(employee);

                var response = new EmployeeShortResponse()
                {
                    Id = employee.Id,
                    Email = employee.Email,
                    FullName = employee.FullName
                };

                return Ok(response);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "CreateEmployee Error");
            }
        }

        /// <summary>
        /// Изменить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Employee>> UpdateEmployee(Guid id, EmployeeCreateUpdateRequest request)
        {
            try
            {
                if (request == null)
                    return BadRequest();

                var employeeToUpdate = await _employeeRepository.GetByIdAsync(id);

                if (employeeToUpdate == null)
                    return NotFound($"Employee {id} doesn't found");

                employeeToUpdate.FirstName = request.FirstName;
                employeeToUpdate.LastName = request.LastName;
                employeeToUpdate.Email = request.Email;
                employeeToUpdate.AppliedPromocodesCount = request.AppliedPromocodesCount;
                employeeToUpdate.Roles.Clear();
                employeeToUpdate.Roles = request.Roles.Select(x => new Role()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

                await _employeeRepository.UpdateAsync(employeeToUpdate);

                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "UpdateEmployee Error");
            }
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployee(Guid id)
        {
            try
            {
                var employeeToDelete = await _employeeRepository.GetByIdAsync(id);

                if (employeeToDelete == null)                
                    return NotFound($"Employee {id} doesn't found");

                await _employeeRepository.DeleteAsync(id);

                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "DeleteEmployee Error");
            }
        }
    }
}